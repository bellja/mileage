#! /usr/bin/perl
 
use CGI;

my $q = new CGI;

# This script is executed both to initially display the form,
# to request which component is desired,
# and again after the user enters a component. 
# Examining the $q->param() tells us which time this is.

print $q->header();

# Lets output the form

print $q->start_form();
print "Enter distance traveled in miles: ";
print $q->textfield(-name => "se"), "\n";
print '<br>' , "\n";
print "Enter gallons of fuel used: ";
print $q->textfield(-name => "te"), "\n"; 
print '<br>' , "\n";
print $q->submit(-value => "compute");
print $q->end_form();

# If $q->param() equals "", this is the first time through.

if ($q->param() != "") {

        my $d1 = $q->param("se");
	my $d2 = $q->param("te");
	$d3 = ($d1 / $d2);
        print "you're currently getting $d3 miles per gallon. \n";

}

print $q->end_html();

<EOF>
